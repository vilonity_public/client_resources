phrase(discord_link);
phrase(support_link);
phrase(website_url);

namespace sign
{
	phrase(login_or_email);
	phrase(login_empty);
	phrase(login_email_is_not_valid);
	phrase(login_is_too_short);

	phrase(password);
	phrase(password_is_empty);
	phrase(password_is_too_short);

	phrase(forgot_password);
	phrase(sign_in_browser);
	phrase(sign_in_browser_cancel);
	phrase(sign_in);

	phrase(outdated_version);
	phrase(user_not_found);
	phrase(user_banned);
	phrase(wrong_password);
	phrase(server_error);

	phrase(rollback_key);
	phrase(rollback_key_def_group);
	phrase(rollback_key_buy);
	phrase(rollback_key_canceled);
	phrase(rollback_key_key);
}
namespace products
{
	phrase(choose_game);

	phrase(sub_status);
	phrase(sub_status_none);
	phrase(sub_status_active);
	phrase(sub_status_frozen);
	phrase(sub_status_lifetime);

	phrase(sub_time);

	namespace times
	{
		phrase(time_sec1);
		phrase(time_sec2);
		phrase(time_sec3);
		phrase(time_min1);
		phrase(time_min2);
		phrase(time_min3);
		phrase(time_hour1);
		phrase(time_hour2);
		phrase(time_hour3);
		phrase(time_days1);
		phrase(time_days2);
		phrase(time_days3);
		phrase(time_month1);
		phrase(time_month2);
		phrase(time_month3);
		phrase(time_years1);
		phrase(time_years2);
		phrase(time_years3);
	}

	phrase(contact_support);
	phrase(buy_subscription);

	phrase(launch_status_start);
	phrase(launch_status_loading);
	phrase(launch_status_loading2);
	phrase(launch_status_starting);
	phrase(launch_status_starting2);
	phrase(launch_status_waiting);

	phrase(launch_error_header);
	phrase(launch_error_closed);
	phrase(launch_error_admin);
	phrase(launch_error_create);
	phrase(launch_error_timeout);
	phrase(launch_error_update);
}
namespace widget
{
	phrase(or_);
	phrase(press);
	phrase(to_assign);
	phrase(to_reset);

	phrase(cancel);
	phrase(continue_);
	phrase(close);
}
namespace legacy_upd
{
	phrase(legacy_upd_header);
	phrase(legacy_upd_header2);
	phrase(legacy_upd_text);
	phrase(legacy_upd_text2);
}
