@echo off
cd /d "%~dp0"

set ec=0

set "phrases_path=..\localization"

for /f "tokens=*" %%a in ('dir /b "%phrases_path%"') do ^
powershell -Command "try { Get-Content '%phrases_path%\%%a' | ConvertFrom-Json | Out-Null; exit 0 } catch { Write-Host '%phrases_path%\%%a' ($_.Exception.Message -split '\r?\n')[0]; exit 1 }" || set ec=1

for /f "tokens=*" %%a in ('dir /b "%phrases_path%"') do ^
for /f "tokens=2 delims=()" %%b in (phrases.h) do ^
findstr "^..%%b.*" "%phrases_path%\%%a">nul || ^
echo phrase "%%b" not found in "%%a" && set ec=1

echo.

set "textures_dir=..\resources\textures"
for /f "tokens=1-3 delims=(,) eol=*" %%a in (textures.h) do ^
call :check_texture "%%b"%%c || set ec=1

exit /b %ec%

:check_texture
set i=0
:for_start
set /a i+=1
if %i% gtr 4 exit /b 0

if not exist "%textures_dir%\s%i%\%~2" ^
echo texture "%~1" not found at "s%i%/%~2"

goto :for_start
